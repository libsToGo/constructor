package constructor

import (
	"flag"
	"fmt"
	"os"
	"reflect"
	"strconv"
	"time"

	"gopkg.in/yaml.v3"
)

// SetDefaults sets default values for fields in a struct.
// If a field has a "default" tag, its value is set to the value specified in the tag.
// If the field is a struct, setDefaults is called recursively on the field.
//
// Example:
//
//	type MyStruct struct{
//		Value string `default:"defaultValue"`
//	}
//	myStruct = MyStruct{}
//	SetDefaults(&myStruct)
func SetDefaults(s interface{}) {
	structValue := reflect.ValueOf(s).Elem()
	structType := structValue.Type()

	for i := 0; i < structValue.NumField(); i++ {
		field := structValue.Field(i)
		fieldType := structType.Field(i)

		defaultValue := fieldType.Tag.Get("default")
		if defaultValue != "" {
			setValueFromString(field, defaultValue)
		} else if fieldType.Type.Kind() == reflect.Struct {
			SetDefaults(field.Addr().Interface())
		}
	}
}

// Set a reflect value from a string it also does the paring
func setValueFromString(field reflect.Value, value string) {
	switch field.Kind() {
	case reflect.String:
		field.SetString(value)
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		intValue, _ := strconv.ParseInt(value, 10, 64)
		field.SetInt(intValue)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		uintValue, _ := strconv.ParseUint(value, 10, 64)
		field.SetUint(uintValue)
	case reflect.Float32, reflect.Float64:
		floatValue, _ := strconv.ParseFloat(value, 64)
		field.SetFloat(floatValue)
	case reflect.Bool:
		boolValue, _ := strconv.ParseBool(value)
		field.SetBool(boolValue)
	case reflect.Struct:
		if field.Type().String() == "time.Time" {
			timeValue, _ := time.Parse(time.RFC3339, value)
			field.Set(reflect.ValueOf(timeValue))
		}
	default:
		// Unsupported type
	}
}

// OverrideFromYAML loads the values from a YAML file and overrides the struct fields accordingly.
// please do note that yaml tags from gopkg.in/yaml.v2 can be used
//
// Example:
//
//	type MyStruct struct{
//		Value string `default:"defaultValue"`
//	}
//	myStruct = MyStruct{}
//	SetDefaults(&myStruct)
//	err := OverrideFromYAML(myStruct, "pathToYaml")
func OverrideFromYAML(s interface{}, filePath string) error {
	yamlData, err := os.ReadFile(filePath)
	if err != nil {
		return err
	}

	var tempMap map[string]interface{}
	if err := yaml.Unmarshal(yamlData, &tempMap); err != nil {
		return err
	}

	if err := yaml.Unmarshal(yamlData, s); err != nil {
		return err
	}

	// updateFieldsFromMap(s, tempMap)
	return nil
}

// updateFieldsFromMap updates the struct fields recursively with the values from the map.
func updateFieldsFromMap(s interface{}, values map[string]interface{}) {
	structValue := reflect.ValueOf(s).Elem()
	structType := structValue.Type()

	for i := 0; i < structValue.NumField(); i++ {
		field := structValue.Field(i)
		fieldType := structType.Field(i)
		fieldName := fieldType.Name

		if !field.CanSet() {
			continue
		}

		if fieldValue, ok := values[fieldName]; ok {
			switch field.Kind() {
			case reflect.Struct:
				if field.Type() == reflect.TypeOf(time.Time{}) {
					// Handle special case for time.Time fields
					if strVal, ok := fieldValue.(string); ok {
						if t, err := time.Parse(time.RFC3339, strVal); err == nil {
							field.Set(reflect.ValueOf(t))
						}
					}
				} else {
					if nestedMap, ok := fieldValue.(map[interface{}]interface{}); ok {
						// Convert nested map to map[string]interface{}
						strMap := make(map[string]interface{})
						for key, val := range nestedMap {
							strMap[fmt.Sprintf("%v", key)] = val
						}
						updateFieldsFromMap(field.Addr().Interface(), strMap)
					}
				}
			default:
				if reflect.TypeOf(fieldValue).ConvertibleTo(field.Type()) {
					field.Set(reflect.ValueOf(fieldValue).Convert(field.Type()))
				}
			}
		}
	}
}

// OverrideFromEnv overrides struct field values with environment variable values.
// If a field has a "env" tag, its value gets overwritted with the enviromental
// varaible specified in the tat
//
// Example:
//
//	type MyStruct struct{
//		Value string `default:"defaultValue", env:"VALUE"`
//	}
//	myStruct = MyStruct{}
//	SetDefaults(&myStruct)
//	OverrideFromEnv(myStruct)
func OverrideFromEnv(s interface{}) {
	structValue := reflect.ValueOf(s).Elem()
	structType := structValue.Type()

	for i := 0; i < structValue.NumField(); i++ {
		field := structValue.Field(i)
		fieldType := structType.Field(i)

		if fieldType.Type.Kind() == reflect.Struct {
			OverrideFromEnv(field.Addr().Interface())
		}
		envName := fieldType.Tag.Get("env")

		if envName != "" {
			envValue := os.Getenv(envName)
			if envValue != "" {
				setValueFromString(field, envValue)
			}
		}
	}
}

// OverrideFromFlags overrides struct field values with flag values.
// If a field has a "env" tag, its value gets overwritted with the enviromental
// varaible specified in the tat
//
// Example:
//
//	type MyStruct struct{
//		Value string `default:"defaultValue", flag:"value" definition:"definition of the flag goes here"`
//	}
//	myStruct = MyStruct{}
//	SetDefaults(&myStruct)
//	flags := OverrideFromFlags(&myStruct)
//	args := [] {
//	"-value","newValue"
//	}
//	flags.Parse(args)
func OverrideFromFlags(s interface{}) *flag.FlagSet {
	myFlags := flag.NewFlagSet("f1", flag.ContinueOnError)
	overrideFromFlags(s, myFlags)
	return myFlags
}

func overrideFromFlags(s interface{}, myFlags *flag.FlagSet) {
	structValue := reflect.ValueOf(s).Elem()
	structType := structValue.Type()

	for i := 0; i < structValue.NumField(); i++ {
		field := structValue.Field(i)
		fieldType := structType.Field(i)

		if fieldType.Type.Kind() == reflect.Struct {
			overrideFromFlags(field.Addr().Interface(), myFlags)
		}
		flagName := fieldType.Tag.Get("flag")
		flagDefinition := fieldType.Tag.Get("definition")

		if flagName != "" && flagDefinition != "" {
			switch field.Kind() {
			case reflect.String:
				myFlags.StringVar((*string)(field.Addr().UnsafePointer()), flagName, field.String(), flagDefinition)

			case reflect.Int:
				myFlags.IntVar((*int)(field.Addr().UnsafePointer()), flagName, int(field.Int()), flagDefinition)
			case reflect.Int8:
				panic("type int8 not suported by flags")
				// myFlags.Int8Var((*int8)(field.Addr().UnsafePointer()), flagName, int8(field.Int()), flagDefinition)
			case reflect.Int16:
				panic("type int16 not suported by flags")
				// myFlags.Int16Var((*int16)(field.Addr().UnsafePointer()), flagName, int16(field.Int()), flagDefinition)
			case reflect.Int32:
				panic("type int32 not suported by flags")
				// myFlags.Int32Var((*int32)(field.Addr().UnsafePointer()), flagName, int32(field.Int()), flagDefinition)
			case reflect.Int64:
				myFlags.Int64Var((*int64)(field.Addr().UnsafePointer()), flagName, field.Int(), flagDefinition)

			case reflect.Uint:
				myFlags.UintVar((*uint)(field.Addr().UnsafePointer()), flagName, uint(field.Uint()), flagDefinition)
			case reflect.Uint8:
				panic("type uint8 not suported by flags")
				// myFlags.Uint8Var((*uint8)(field.Addr().UnsafePointer()), flagName, uint8(field.Uint()), flagDefinition)
			case reflect.Uint16:
				panic("type uint16 not suported by flags")
				// myFlags.Uint16Var((*uint16)(field.Addr().UnsafePointer()), flagName, uint16(field.Uint()), flagDefinition)
			case reflect.Uint32:
				panic("type uint32 not suported by flags")
				// myFlags.Uint32Var((*uint32)(field.Addr().UnsafePointer()), flagName, uint32(field.Uint()), flagDefinition)
			case reflect.Uint64:
				myFlags.Uint64Var((*uint64)(field.Addr().UnsafePointer()), flagName, field.Uint(), flagDefinition)
			case reflect.Float32:
				panic("type float32 not suported by flags")
				// myFlags.Float32Var((*float32)(field.Addr().UnsafePointer()), flagName, float32(field.Float()), flagDefinition)
			case reflect.Float64:
				myFlags.Float64Var((*float64)(field.Addr().UnsafePointer()), flagName, field.Float(), flagDefinition)

			case reflect.Bool:
				myFlags.BoolVar((*bool)(field.Addr().UnsafePointer()), flagName, field.Bool(), flagDefinition)
			}
		}
	}
}
