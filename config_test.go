package constructor

import (
	"os"
	"testing"
)

type MyNestedStruct struct {
	NestedField string `default:"nested_default" env:"NESTED_FIELD" flag:"nested" definition:"nested string" yaml:"nested_field"`
}

type MyStructWithNested struct {
	Field1 string         `default:"default_value1" env:"FIELD1" flag:"f1" definition:"field1 string" yaml:"a_field1"`
	Field2 MyNestedStruct `yaml:"nested_data"`
	Field3 bool           `default:"true" env:"FIELD3" env:"FIELD3" flag:"f3" definition:"field3 bool" yaml:"Field3"`
	Field4 float32        `default:"3.14" env:"FIELD4" yaml:"Field4"`
	Field5 uint           `default:"10" env:"FIELD5"`
}

func TestPopulateStructWithDefaultValues(t *testing.T) {
	var s MyStructWithNested
	SetDefaults(&s)

	expected := MyStructWithNested{
		Field1: "default_value1",
		Field2: MyNestedStruct{"nested_default"},
		Field3: true,
		Field4: 3.14,
		Field5: 10,
	}

	if s != expected {
		t.Errorf("Default expected: %v, got %v", expected, s)
	}
}

func TestPopulateStructWithDefaultValues_NoDefaults(t *testing.T) {
	type MyStructNoDefaults struct {
		Field1 string
		Field2 int
		Field3 bool
	}

	var s MyStructNoDefaults
	SetDefaults(&s)

	expectedField1 := ""
	if s.Field1 != expectedField1 {
		t.Errorf("Field1: expected %s, got %s", expectedField1, s.Field1)
	}

	expectedField2 := 0
	if s.Field2 != expectedField2 {
		t.Errorf("Field2: expected %d, got %d", expectedField2, s.Field2)
	}

	expectedField3 := false
	if s.Field3 != expectedField3 {
		t.Errorf("Field3: expected %t, got %t", expectedField3, s.Field3)
	}
}

func TestPopulateStructWithDefaultValues_NestedStruct(t *testing.T) {
	var s MyStructWithNested
	SetDefaults(&s)

	expectedField1 := "default_value1"
	if s.Field1 != expectedField1 {
		t.Errorf("Field1: expected %s, got %s", expectedField1, s.Field1)
	}

	expectedNestedField := "nested_default"
	if s.Field2.NestedField != expectedNestedField {
		t.Errorf("Field2.NestedField: expected %s, got %s", expectedNestedField, s.Field2.NestedField)
	}

	expectedField3 := true
	if s.Field3 != expectedField3 {
		t.Errorf("Field3: expected %t, got %t", expectedField3, s.Field3)
	}

	expectedField4 := float32(3.14)
	if s.Field4 != expectedField4 {
		t.Errorf("Field4: expected %f, got %f", expectedField4, s.Field4)
	}

	expectedField5 := uint(10)
	if s.Field5 != expectedField5 {
		t.Errorf("Field5: expected %d, got %d", expectedField5, s.Field5)
	}
}

func TestLoadFromYAMLFile(t *testing.T) {
	// Create a temporary YAML file with test data
	yamlData := []byte(`
a_field1: overridden_value1
nested_data:
  nested_field: overridden_nested_value
Field3: false
Field4: 1.23
Nope: nope
`)

	tmpFile, err := os.CreateTemp("", "config*.yaml")
	if err != nil {
		t.Fatalf("Failed to create temporary YAML file: %v", err)
	}
	defer os.Remove(tmpFile.Name())

	if _, err := tmpFile.Write(yamlData); err != nil {
		t.Fatalf("Failed to write data to temporary YAML file: %v", err)
	}
	if err := tmpFile.Close(); err != nil {
		t.Fatalf("Failed to close temporary YAML file: %v", err)
	}

	// Define the expected struct with overridden values
	expectedConfig := MyStructWithNested{
		Field1: "overridden_value1",
		Field2: MyNestedStruct{
			NestedField: "overridden_nested_value",
		},
		Field3: false,
		Field4: 1.23,
		Field5: 10,
	}

	// load with default data
	// Load values from the YAML file
	var config MyStructWithNested
	SetDefaults(&config)

	err = OverrideFromYAML(&config, tmpFile.Name())
	if err != nil {
		t.Fatalf("Failed to load values from YAML file: %v", err)
	}

	// Compare the loaded config with the expected values
	if config != expectedConfig {
		t.Errorf("Loaded config does not match the expected values:\nExpected: %+v\nGot     : %+v", expectedConfig, config)
	}
}

func TestOverrideFromEnv(t *testing.T) {
	// Set up environment variables
	os.Setenv("FIELD1", "override_value1")
	os.Setenv("NESTED_FIELD", "nested_override")

	// Create instance of MyStructWithNested
	var config MyStructWithNested
	SetDefaults(&config)

	// Override values from environment variables
	OverrideFromEnv(&config)
	if config.Field1 != "override_value1" {
		t.Errorf("Expected config.Field1 to be 'override_value1', but got '%s'", config.Field1)
	}
	if config.Field2.NestedField != "nested_override" {
		t.Errorf("Expected config.Field2.NestedField to be 'nested_override', but got '%s'", config.Field2.NestedField)
	}

	// Clean up environment variables
	os.Unsetenv("FIELD1")
}

func TestOverrideFromFlags(t *testing.T) {
	config := MyStructWithNested{}
	expectedConfig := MyStructWithNested{}

	SetDefaults(&config)
	SetDefaults(&expectedConfig)
	flags := OverrideFromFlags(&config)

	args := []string{
		"--f1", "flagged_value1",
		"-nested", "nested_flagged",
	}

	flags.Parse(args)

	if config.Field1 != args[1] {
		t.Errorf("expected config.Field1 to be '%s' but got '%s'", args[1], config.Field1)
	}

	if config.Field2.NestedField != args[3] {
		t.Errorf("expected config.Field2.NestedField to be '%s' but got '%s'", args[3], config.Field2.NestedField)
	}
}

func TestOverrideFromFlagsPanic(t *testing.T) {
	testCases := []struct {
		name        string
		shouldPanic bool
		structVal   interface{}
	}{
		{"int8", true, &struct {
			V int8 `default:"10" flag:"i8" definition:"an 8-bit integer"`
		}{}},
		{"int16", true, &struct {
			V int16 `default:"10" flag:"i16" definition:"a 16-bit integer"`
		}{}},
		{"int32", true, &struct {
			V int32 `default:"10" flag:"i32" definition:"a 32-bit integer"`
		}{}},
		{"uint8", true, &struct {
			V uint8 `default:"10" flag:"u8" definition:"an 8-bit unsigned integer"`
		}{}},
		{"uint16", true, &struct {
			V uint16 `default:"10" flag:"u16" definition:"a 16-bit unsigned integer"`
		}{}},
		{"uint32", true, &struct {
			V uint32 `default:"10" flag:"u32" definition:"a 32-bit unsigned integer"`
		}{}},
		{"float32", true, &struct {
			V float32 `default:"10.10" flag:"f32" definition:"a 32-bit floating point"`
		}{}},

		{"string", false, &struct {
			V string `default:"value" flag:"v" definition:"a string"`
		}{}},
		{"int", false, &struct {
			V int `default:"10" flag:"i" definition:"an integer"`
		}{}},
		{"int64", false, &struct {
			V int64 `default:"10" flag:"i64" definition:"a 64-bit integer"`
		}{}},
		{"uint", false, &struct {
			V uint `default:"10" flag:"u" definition:"an unsigned integer"`
		}{}},
		{"uint64", false, &struct {
			V uint64 `default:"10" flag:"u64" definition:"a 64-bit unsigned integer"`
		}{}},
		{"float64", false, &struct {
			V float64 `default:"10.10" flag:"f64" definition:"a 64-bit floating point"`
		}{}},
		{"bool", false, &struct {
			V bool `default:"true" flag:"b" definition:"a boolean value"`
		}{}},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			defer func() {
				r := recover()
				if r == nil && tc.shouldPanic {
					t.Errorf("The code did not panic for %s but it should have", tc.name)
				}

				if r != nil && !tc.shouldPanic {
					t.Errorf("The code should not panic for %s but it did", tc.name)
				}
			}()

			SetDefaults(tc.structVal)
			myFlags := OverrideFromFlags(tc.structVal)
			_ = myFlags
		})
	}
}
