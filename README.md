# Constructor

Simplify the configuration structs in Go by using tags, setting default values, and allowing for overrides from YAML files, environment variables, and flags.

## Use Case

Imagine you're developing a solution in Go, and you want a straightforward way to manage its configuration. You need to set default values but also allow for easy overrides from environment variables and configuration files. This process can be streamlined by setting a couple of tags and adding minimal lines of code to establish priority flow.

With Constructor, you can effortlessly:

    Set Defaults: Define default values for your configuration parameters with tags.
    Load from YAML: Allow your configuration to be extended or overridden through a YAML file, providing flexibility.
    Environmental Overrides: Enable easy customization by allowing overrides from environment variables.
    Command Line Flags: Further enhance configurability by accepting command line flags.

These enhancements ensure that configuring your solution becomes a seamless and efficient process, requiring minimal effort for maximum flexibility.


## Installation
```bash
go get gitlab.com/libsToGo/constructor

```

## Getting Started / examples

### 1. Setting struct default values with tags.

For a basic start to set the default values of a struct according to tags.

```go

type myConfig struct {
	Version  int    `default:"1"`
	LogLevel string `default:"dev"`
}

func main() {
	//create the struct with default values
	config := myConfig{}
	constructor.SetDefaults(&config)
}
```

### 2. Overriding with YAML

We can override the current values in the struct from a yaml file.

```yaml
Version: 2
LogLevel: "prod"
```

```go
package main

import (
	"log"

	"gitlab.com/libsToGo/constructor"
)

type myConfig struct {
	Version  int    `default:"1"`
	LogLevel string `default:"dev"`
}

func main() {
	//create the struct with default
	config := myConfig{}
	constructor.SetDefaults(&config)

	//override the default from yaml if present
	err := constructor.OverrideFromYAML(&config, "myYaml.yaml")
	if err != nil {
        :w
        log.Fatal(err)
	}
}
```

### 3. Overriding with environmental variables

We can override the current values in the struct from environmental variables.

```bash
export LOG_LEVEL=staging
export VER=3
```

```go
package main

import (
	"log"

	"gitlab.com/libsToGo/constructor"
)

type myConfig struct {
	Version  int    `default:"1" env:"VER"`
	LogLevel string `default:"dev" env:"LOG_LEVEL"`
}

func main() {
	//create the struct with default
	config := myConfig{}
	constructor.SetDefaults(&config)

	//override the default from yaml if present
	err := constructor.OverrideFromYAML(&config, "myYaml.yaml")
	if err != nil {
		log.Fatal(err)
	}

	//now we can override everything with env variables
    constructor.OverrideFromEnv(&config)
}
```

### 4. overriding with flags

We can override the current values in the struct with flags/arguments.

```go
package main

import (
	"log"
	"os"

	"gitlab.com/libsToGo/constructor"
)

type myConfig struct {
	Version  int    `default:"1" env:"VER"`
	LogLevel string `default:"dev" env:"LOG_LEVEL" flag:"l" definition:"the log level"`
}

func main() {
	//create the struct with default
	config := myConfig{}
	constructor.SetDefaults(&config)

	//override the default from yaml if present
	err := constructor.OverrideFromYAML(&config, "myYaml.yaml")
	if err != nil {
		log.Fatal(err)
	}

	//now we can override everything with env variables
	constructor.OverrideFromEnv(&config)

	//overide with flags
	flags := constructor.OverrideFromFlags(&config)
	flags.Parse(os.Args[1:])
	flags.PrintDefaults()
}
```

```bash
go run main.go -l dev
```

### 5. Hail Mary with nested values

This is a more complete example of how i tend to use it in production with nested structs

```yaml
logger:
  level: "prod"
  pretty: false
```

```go
package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/libsToGo/constructor"
)

type myConfig struct {
	Version int     `default:"1"`
	Logging Logging `yaml:"logger"`
}

type Logging struct {
	Level  string `default:"dev" yaml:"level" env:"LEVEL" flag:"l" definition:"change the log level"`
	Pretty bool   `default:"true" yaml:"pretty" env:"PRETTY" flag:"p" definition:"enable pretty log level"`
}

func main() {
	// create the struct with default
	config := myConfig{}
	constructor.SetDefaults(&config)
	fmt.Printf("default: %+v\n", config)

	// override the default from yaml if present
	err := constructor.OverrideFromYAML(&config, "myYaml.yaml")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("overridden from yaml:  %+v\n", config)

	// overide from env varaibles
	constructor.OverrideFromEnv(&config)
	fmt.Printf("overridden from environtmental varaibles:  %+v\n", config)

	// override from flags
	flags := constructor.OverrideFromFlags(&config)
	flags.Parse(os.Args[1:])
	// flags.PrintDefaults()
	fmt.Printf("overridden from flags:  %+v\n", config)
}
```

```bash
$ export LEVEL=info
$ go run . -p -l=warn

default: {Version:1 Logging:{Level:dev Pretty:true}}
overridden from yaml:  {Version:1 Logging:{Level:prod Pretty:false}}
overridden from environtmental varaibles:  {Version:1 Logging:{Level:info Pretty:false}}
overridden from flags:  {Version:1 Logging:{Level:warn Pretty:true}}

```



## the TAGS

| Tag     | Example                   | Description                                                  |
| ------- | ------------------------- | ------------------------------------------------------------ |
| default | default:"a_default value" | used to set the default value for the member of a struct     |
| yaml    | yam:"a_yaml_key"          | The key inside a yaml files used to override the current value of a member in a struct |
| env     | env:"a_env_varaible"      | The environmental varaible names used to override the current value of a member in a struct |



